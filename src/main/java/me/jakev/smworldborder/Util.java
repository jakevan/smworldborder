package me.jakev.smworldborder;

import java.util.Random;

/**
 * Created by Jake on 10/19/2020.
 * <insert description here>
 */
public class Util {
    private static int count = 0;
    public static String getMessage(){
        count++;
        switch (count){
            case 0: return "The play area is limited to 3x3x3 systems";
            case 1: return "FTL is disabled";
            case 2: return "Spawn blueprints with credits";
            case 3: return "Only 1% reactor stabilization is needed";
            case 4: return "Owning a system gives 4x as much credits";
            case 5: return "Claim a system by having the most amount of reactor blocks between all stations in that system.";
            case 6: return "Type !recycle to turn your ship into credits!";
            default: count = -1; return "Mining asteroids gives you credits AND blocks.";
        }
    }
    private static Random random = new Random();
    public static int randInt(int min, int max){
        return min + random.nextInt(max-min);
    }
}
